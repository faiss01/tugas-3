from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('shopping/', include('shopping.urls')),
    path('shopingyuk/', include('shopingyuk.urls')),
    path('shopingkuy/', include('shopingkuy.urls')),
]
