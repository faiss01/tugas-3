from django.shortcuts import render

def index(request):
    return render(request, 'shopyuk/yuk.html')
